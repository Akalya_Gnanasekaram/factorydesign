package bcas.ap.dp.fact;

public class Square implements Shape {

	@Override
	public void draw() {
		System.out.println("draw method from square");
	}

	@Override
	public double getArea(double a, double h) {
		return a * h;
	}

	@Override
	public double getPerimeter(double w) {
		return 4 * w;
	}

	@Override
	public double getPerimeter(double w, double h) {

		return 0;
	}

	@Override
	public double getareaOfCircle(double radious) {

		return 0;
	}

	@Override
	public double getperimeterOfCircle(double radious) {

		return 0;
	}

	@Override
	public double getareaOfPentagon(double a, double h) {

		return 0;
	}

	@Override
	public double getperimeterOfPentagon(double a) {

		return 0;
	}

}