package bcas.ap.dp.fact;

public class ShapeFactory {

	public Shape callShape(ShapeType shapeType) {

		if (shapeType.equals(ShapeType.CIRCLE)) {
			return new Circle();
		}

		if (shapeType.equals(ShapeType.SQUARE)) {
			return new Square();
		}

		if (shapeType.equals(ShapeType.RECTANGLE)) {
			return new Rectangle();
		}

		if (shapeType.equals(ShapeType.RECTANGLE)) {
			return new Rectangle();
		}

		if (shapeType.equals(ShapeType.PENTAGON)) {
			return new Pentagon();
		}
		return null;
	}
}