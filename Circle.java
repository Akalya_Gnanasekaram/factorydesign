package bcas.ap.dp.fact;

public class Circle implements Shape {

	@Override
	public void draw() {
		System.out.println("draw method from circle");
	}

	@Override
	public double getareaOfCircle(double radious) {
		return Math.PI * Math.pow(radious, 2);
	}

	@Override
	public double getperimeterOfCircle(double radious) {
		return 2 * Math.PI * Math.pow(radious, 2);
	}

	@Override
	public double getArea(double a, double h) {

		return 0;
	}

	@Override
	public double getPerimeter(double w, double h) {

		return 0;
	}

	@Override
	public double getPerimeter(double w) {

		return 0;
	}

	@Override
	public double getareaOfPentagon(double a, double h) {
		return 0;
	}

	@Override
	public double getperimeterOfPentagon(double a) {

		return 0;
	}

}