package bcas.ap.dp.fact;

public class ShapeFactoryDemo {

	public static void main(String[] args) {

		ShapeFactory shapefactory = new ShapeFactory();

		Shape circle = shapefactory.callShape(ShapeType.CIRCLE);
		circle.draw();
		Circle cir = new Circle();
		System.out.println("Circle area is " + cir.getareaOfCircle(7) +"\n"+"Circle perimeter is "
				+ cir.getperimeterOfCircle(14) + "\n\n");

		Shape rectangle = shapefactory.callShape(ShapeType.RECTANGLE);
		rectangle.draw();
		Rectangle rec = new Rectangle();
		System.out.println("Rectangle area is " + rec.getArea(20,30) + "\n" + "Rectangle perimeter is "
				+ rec.getPerimeter(10, 20) + "\n\n");

		Shape square = shapefactory.callShape(ShapeType.SQUARE);
		square.draw();
		Square sqr = new Square();
		System.out.println("Square area is " + sqr.getArea(50,50) + "\n" + "Square perimeter is "
				+ sqr.getPerimeter(10) + "\n\n");

		Shape pentagon = shapefactory.callShape(ShapeType.PENTAGON);
		pentagon.draw();
		Pentagon pen = new Pentagon();
		System.out.println("Pentagon area is " + pen.getareaOfPentagon(8,16) + "\n" + "Pentagon perimeter is "+ pen.getperimeterOfPentagon(6) + "\n\n");

	}
}