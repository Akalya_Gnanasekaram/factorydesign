package bcas.ap.dp.fact;

public class Pentagon implements Shape {

	@Override
	public void draw() {
		System.out.println("draw method from pentagon");
	}

	@Override
	public double getareaOfPentagon(double a, double h) {
		return 5 / 2 * a * h;
	}

	@Override
	public double getperimeterOfPentagon(double a) {
		return 5 * a;
	}

	@Override
	public double getArea(double a, double h) {

		return 0;
	}

	@Override
	public double getPerimeter(double w, double h) {

		return 0;
	}

	@Override
	public double getPerimeter(double w) {

		return 0;
	}

	@Override
	public double getareaOfCircle(double radious) {

		return 0;
	}

	@Override
	public double getperimeterOfCircle(double radious) {

		return 0;
	}

}