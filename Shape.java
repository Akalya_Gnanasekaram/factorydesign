package bcas.ap.dp.fact;

public interface Shape {
	
	public void draw();
	
	public double getArea(double a, double h);

	public double getPerimeter(double w, double h);

	double getPerimeter(double w);

	double getareaOfCircle(double radious);

	double getperimeterOfCircle(double radious);
	
	double getareaOfPentagon(double a, double h);
	
	double getperimeterOfPentagon(double a);

}